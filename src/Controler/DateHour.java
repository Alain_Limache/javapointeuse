package Controler;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

/**
 *  Classe qui stocke 
 *  - La date du jour
 *  - L'heure à la seconde près 
 *  - L'heure arrondi au quart d'heure près
 */
public class DateHour {
	
	//------ ATTRIBUTS ------//
	
	private LocalDate today;
	private LocalTime HourRounded;
	private LocalTime hour;

	 //------ CONSTRUCTEUR ------//

	 /**
	 *  Contructeur par défaut 
	 * Initialise 
	 * - le jour 
	 * - une heure à la seconde près 
	 * - une heure au quart d'heure près
	 */
	public DateHour() {
		getTodayDate();
		getHour();
		RoundedToQuarter();
	}


	//------ METHODES ------//
	/**
	Methode pour avoir la date actuelle 
	 * @return LocalDate today 
	 */
	public LocalDate getTodayDate() {
		today = LocalDate.now();
		return today; 
	}
	
	/**
	Methode pour avoir l'heure actuelle 
	 * @return LocalTime hour 
	 */
	public LocalTime getHour() {
		hour = LocalTime.now();
		hour = LocalTime.now().truncatedTo(ChronoUnit.SECONDS);
		return hour; 
	}

	
	/** 
	 * Methode pour arrondir l'heure au quart d'heure le plus proche 
	 * @return LocalTime HourRounded 
	 */
	
	public LocalTime RoundedToQuarter() {
	
		int minute = LocalTime.now().getMinute(); 
		int second = LocalTime.now().getSecond(); 
		 	
		if ((minute >= 0) && (minute <= 15)) {
			if ((minute < 7) || (minute == 7 && second < 50)) {
				HourRounded = LocalTime.now().truncatedTo(ChronoUnit.HOURS).plusMinutes(15*(LocalTime.now().getMinute() / 15));
			}
			else {
				HourRounded = LocalTime.now().truncatedTo(ChronoUnit.HOURS);
				HourRounded = HourRounded.plusMinutes(15);
			}
		}
		if ((minute > 15) && (minute <= 30)) {
			if ((minute < 22) || (minute == 22 && second < 50)) {
				HourRounded = LocalTime.now().truncatedTo(ChronoUnit.HOURS).plusMinutes(15*(LocalTime.now().getMinute() / 15)); 
			}
			else {
				HourRounded = LocalTime.now().truncatedTo(ChronoUnit.HOURS);
				HourRounded = HourRounded.plusMinutes(30);
			}
		}
		if ((minute > 30) && (minute <= 45)) {
			if ((minute < 37) || (minute == 37 && second < 50)) {
				HourRounded = LocalTime.now().truncatedTo(ChronoUnit.HOURS).plusMinutes(15*(LocalTime.now().getMinute() / 15));
			}
			else {
				HourRounded = LocalTime.now().truncatedTo(ChronoUnit.HOURS);
				HourRounded = HourRounded.plusMinutes(45);
			}
		}
		if ((minute > 45) && (minute <= 59)) {
			if ((minute < 52) || (minute == 52 && second < 50)) {
				HourRounded = LocalTime.now().truncatedTo(ChronoUnit.HOURS).plusMinutes(15*(LocalTime.now().getMinute() / 15));
			}
			else {
				HourRounded = LocalTime.now().truncatedTo(ChronoUnit.HOURS);
				HourRounded = HourRounded.plusHours(1);

			}
		}
	
		return HourRounded;
	}
	
	
}



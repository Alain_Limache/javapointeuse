package Controler;

import java.io.Serializable;
import java.util.ArrayList;

import Model.ConvertEmployee;
import Model.Employee;


/**
*  Classe qui stocke 
*  - une liste de ConvertEmployee qui contient le nom, prenom et id d'un employé à envoyer à la pointeuse
*/
public class ConvertListEmployee implements Serializable{ 
	
	 //------ ATTRIBUT ------//
	private static final long serialVersionUID = 1L;
	private ArrayList<ConvertEmployee> newList;
	
	//------ CONSTRUCTEUR ------//
	/**
	* Constructeur
	* Instancie un nouvel objet ConvertListEmployee
	*/
	public ConvertListEmployee() 
	{
		this.newList = new ArrayList<ConvertEmployee>();
	}

	//------ METHODES ------//

	/**
	* Transforme une liste d'employée en une convertEmployee
	* @param paramListEmployee
	*/
	public void addList(ArrayList<Employee> paramListEmployee){	
		for(int i = 0; i< paramListEmployee.size();i++) {
			ConvertEmployee convertedEmployee = new ConvertEmployee(paramListEmployee.get(i));
			this.newList.add(convertedEmployee);
		}
	}
	
	/**
	* Récupérer la liste de convertEmployee
	*/
	public ArrayList<ConvertEmployee> getNewListEmployee(){	
		return this.newList;
	}
	
	
}

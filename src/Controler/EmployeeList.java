package Controler;

import java.util.ArrayList;

import Model.Company;
import Model.Employee;

/**
 *  Classe qui stocke 
 *  - Une entreprise
 *  - la liste des employées de cette entreprise  
 */

public class EmployeeList {
	
	//------ ATTRIBUTS ------//

	private ArrayList<Employee> listEmployee = new ArrayList<Employee>();
	private Company company;
	
	
	//------ Constructeurs ------//

	/**
	Constructeur de recopie pour initialiser l'entreprise à partir d'une autre
	 @param company 
	*/
	
	public EmployeeList(Company company) {
		this.company = company;
		listEmployee = new ArrayList<Employee>();
	}

	//------ METHODES ------//
	
	/**
	 * Méthode de mise à jour de la liste d'employée 
	 * Remplit une liste d'employé avec les employés de tous les departements
	 */
	
	public void update() {
		for (int iCptDep = 0; iCptDep < company.getSizeDepartmentList(); iCptDep++) 
		{
			int sizeEmpList = company.getDepartment(iCptDep).getSizeEmployeeList();
			for (int iCptEmp = 0; iCptEmp < sizeEmpList; iCptEmp++) 
			{
				listEmployee.add(company.getDepartment(iCptDep).getEmployeeAt(iCptEmp));
			}
		}
	}
	
	
	/**
	 * Retourne la taille de la liste d'employés
	  * @return le nombre d'employés total de l'entreprise
	 */
	public int getSize() {
		return listEmployee.size();
	}
	
	
	/**
	 * Retourne l'employé à la position demandée
	 * @param index la position dans la liste d'employé 
	 * @return l'employé à cette position
	 */
	public Employee getEmployee(int index) {
		return listEmployee.get(index);
	}
		
	
}

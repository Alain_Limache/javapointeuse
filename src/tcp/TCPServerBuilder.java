package tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServerBuilder{
	
	//------ATTRIBUTS------// 
	
	ServerSocket ss; // le socket passif
	Socket s; // le socket actif 
	InetSocketAddress isA; // L'adresse IP 
	
	
	//------CONSTRUCTEUR------//	
	
	/**
	  * Instancie un nouvel objet TCPServerBuilder
	  */
	TCPServerBuilder(){
		ss = null;
		s = null;
		isA = null;
	}
	
	
	//------METHODE------//
	
	/**
	 * Initialise le socket avec l'adresse IP Locale et un port par défaut 
	 */
	protected void setSocket() throws IOException {
		isA = new InetSocketAddress("localhost",8085);
		ss = new ServerSocket(isA.getPort());

	}
}

package tcp;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import Model.ConvertEmployee;


public class Client extends TCPClientBuilder implements Runnable {
	
	//------ATTRIBUT------// 
	
	private ArrayList<ConvertEmployee> listEmployees; // liste d'employés 
	
	
	//------CONSTRUCTEUR------//	
	
	/**
	  * Instancie un nouvel objet Client
	  * avec les arguments passés en parametres du constructeur.
	  * @param paramList Liste d'employés à instancier
	  */
	public Client(ArrayList<ConvertEmployee> paramList){
		
		listEmployees = new ArrayList<ConvertEmployee>();
		
		for(int i = 0; i<paramList.size(); i++) {
			listEmployees.add(paramList.get(i));
		}
	}
	
	
	//------METHODE------//
	
	/**
	 * Méthode permettant d'exécuter le client  
	 */
    public void run() {
    	
    	try {
    		// Initialise le socket 
	        setSocket(); 
	        
	        // Initialise un objet flux pour écrire dans le socket
	        OutputStream outputStream = s.getOutputStream();
	        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
	       
	        System.out.println("Sending list of Employees to the time clock work...");
	        
	        // On écrit dans l'objet la liste d'employés
	        objectOutputStream.writeObject(listEmployees);
	
	    	// Ferme le socket 
	        s.close();
	        
    	} catch(IOException e) {
			System.out.println("ERROR ! Sending list of Employees to the time clock work failed. Please restart the application"); 
		} 
    }
}
package tcp;
import java.io.*;
import java.util.ArrayList;
import Model.ConvertEmployee;

public class Server extends TCPServerBuilder implements Runnable {
	
	//------ATTRIBUT------// 

	private ArrayList<ConvertEmployee> listEmployees; // liste d'employés 
	
	
	//------CONSTRUCTEUR------//	
	
	/**
	  * Instancie un nouvel objet Server
	  */
	public Server(){
		
		listEmployees = new ArrayList<ConvertEmployee>();
	}
	
	
	//------METHODES------//
	
	/**
	 * Renvoie la liste d'employés 
	 * @return (ArrayList<ConvertEmployee>) listEmployees - la liste d'employés 
	 */
	public ArrayList<ConvertEmployee> getList() {
		return listEmployees;
	}
	
	
	/**
	 * Méthode permettant d'exécuter le serveur  
	 */
    @SuppressWarnings("unchecked")
	public void run() {
    	try {
    		// Initialise le socket 
	        setSocket();
			
	        // Reception du socket du client
	    	s = ss.accept(); 
	    	
	    	// Initialise un objet flux pour lire dans le socket
	        InputStream inputStream = s.getInputStream();
	        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
		      
	        // Liste temporaire créé à partir de la liste reçue
			ArrayList<ConvertEmployee> tempList = ((ArrayList<ConvertEmployee>) objectInputStream.readObject());
		      
	        // Remplit la liste d'employés des éléments de la liste temporaire
		    for(int i = 0; i<tempList.size(); i++) {
		    	listEmployees.add(tempList.get(i));
		    }
		    
		    // Ferme les sockets
	        ss.close();
	        s.close();

		} catch(IOException | ClassNotFoundException e) {
			//System.out.println("IOException Serveur "); 
		} 
   }
}
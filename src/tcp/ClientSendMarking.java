package tcp;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import Model.ConvertEmployee;
import Model.ListMarkings;

public class ClientSendMarking extends TCPClientBuilder implements Runnable {
	
	//------ATTRIBUT------// 
	
	private HashMap<ConvertEmployee, ListMarkings> listEmployeeMark; // liste d'employés associé à une liste de dates
	
	
	//------CONSTRUCTEUR------//	
	
	/**
	  * Instancie un nouvel objet ClientSendMarking
	  * avec les arguments passés en parametres du constructeur.
	  * @param paramMap liste d'employés associé à une liste de dates à instancier
	  */
	public ClientSendMarking(HashMap<ConvertEmployee,ListMarkings> paramMap){
		
		listEmployeeMark = new HashMap<ConvertEmployee,ListMarkings>();
		
		for(Map.Entry<ConvertEmployee,ListMarkings> entry : paramMap.entrySet()) {
			ConvertEmployee key = entry.getKey();
			ListMarkings value = entry.getValue();
			listEmployeeMark.put(key, value);
		}
		
	}
	
	
	//------METHODE------//
	
	/**
	 * Méthode permettant d'exécuter le client  
	 */
    public void run() {
    	
    	try {
       		// Initialise le socket
	        setSocket();
	
	        // Initialise un objet flux pour écrire dans le socket
	        OutputStream outputStream = s.getOutputStream();
	        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
	        
	        // On écrit dans l'objet la liste d'employés
	        objectOutputStream.writeObject(listEmployeeMark);
	        
	    	// Ferme le socket 
	        s.close();
	        
    	} catch(IOException e) {
			System.out.println("Sending data to application failed, please open the server and try again "); 
		} 
    }
}

package tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

import Model.ConvertEmployee;
import Model.ListMarkings;

public class ServerSendMarking extends TCPServerBuilder implements Runnable {
	
	//------ATTRIBUT------// 
	
	private HashMap<ConvertEmployee,ListMarkings> listEmployeeMark; // liste d'employés associé à une liste de dates
	
	
	//------CONSTRUCTEUR------//	
	
	/**
	  * Instancie un nouvel objet ServerSendMarking
	  */
	public ServerSendMarking(){
		listEmployeeMark = new HashMap<ConvertEmployee,ListMarkings>();
	}
	
	
	//------METHODES------//
	
	/**
	 * Renvoie la liste d'employés 
	 * @return (HashMap<ConvertEmployee,ListMarkings>) listEmployeeMark - la liste d'employés associé à une liste de dates
	 */
	public HashMap<ConvertEmployee,ListMarkings> getList() {
		return listEmployeeMark;
	}
	
	
	/**
	 * Méthode permettant d'exécuter le serveur  
	 */
    @SuppressWarnings({ "unchecked" })
	public void run() {
    	while(true) {
	    	try {
	    		// Initialise le socket 
		        setSocket();
						        
		        // Reception du socket du client
		    	s = ss.accept(); 
		
		    	// Initialise un objet flux pour lire dans le socket
		        InputStream inputStream = s.getInputStream();
		        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
		        
		        // Liste temporaire créé à partir de la liste reçue
		        HashMap<ConvertEmployee,ListMarkings> tempList = ((HashMap<ConvertEmployee,ListMarkings>) objectInputStream.readObject());
		        
		        // Remplit la liste des éléments de la liste temporaire
		        for(Map.Entry<ConvertEmployee,ListMarkings> entry : tempList.entrySet()) {
					ConvertEmployee key = entry.getKey();
					ListMarkings value = entry.getValue();
					listEmployeeMark.put(key, value);
				}
		        
		        System.out.println("IN/OUT");
		        
		        // Ferme les sockets
		        ss.close();
		        s.close();
	
			} catch(IOException | ClassNotFoundException e) {
				//System.out.println("Sending markings to application failed, please try again"); 
			} 
    	}
   }
}

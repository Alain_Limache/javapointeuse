package tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

class TCPClientBuilder{ 
	
	//------ATTRIBUTS------// 
	
	Socket s; // le socket actif 
	InetSocketAddress isA; // L'adresse IP 
	
	
	//------CONSTRUCTEUR------//	
	
	/**
	  * Instancie un nouvel objet TCPClientBuilder
	  */
	TCPClientBuilder() {
		s = null;
		isA = null;
	}
	
	
	//------METHODE------//
	
	/**
	 * Initialise le socket avec l'adresse IP Locale et un port par défaut 
	 */
	protected void setSocket() throws IOException {
		isA = new InetSocketAddress("localhost",8085);
		s = new Socket(isA.getHostName(), isA.getPort());
	}
	
}

package View;

import javax.swing.Timer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.JLabel;
import Controler.DateHour;
import Model.Company;
import Model.ConvertEmployee;
import Model.Employee;
import Model.ListMarkings;
import tcp.ClientSendMarking;
import tcp.Server;

/**
*  Classe représentant la pointeuse qui affiche
*  - l'heure qu'il est 
*  - la date
*  - l'heure arrondie
*  - l'heure de pointage
*  - qui crée une HashMap avec les employés qui ont pointés ainsi que leur pointages 
*  
*/
public class TimeClockWork extends JFrame implements ActionListener{
	
	//------ ATTRIBUTS ------//
	
	Company Company;     		// une compagnie  
	
	JLabel lblHour; 			// un label pour l'heure
	JLabel lblDate;				// un label pour la date
	JLabel lblHourRounded;		// un label pour l'heure arrondie
	JLabel lblError;			// un label s'il y a erreur dans l'heure affichée 
	
	private int cptTimer = 0;							 // un timer
	private JComboBox<ConvertEmployee> comboBoxEmployee; // liste déroulante des employées
	private LocalDateTime markTime; 					 // Date du pointage	
	private ArrayList<ConvertEmployee> listEmployee = new ArrayList<ConvertEmployee>(); // Liste d'employés pour utilisation dans la pointeuse
	private HashMap<ConvertEmployee,ListMarkings> markList = new HashMap<ConvertEmployee,ListMarkings>(); // Liste des pointages
	
	
	//------ CONSTRUCTEUR ------//

	/**
	  * Instancie un nouvel objet TimeClockWork 
	  * initialise une fenêtre qui permet le pointage 
	  */
	public TimeClockWork() {
		
  	  //création de la fenêtre
      setTitle("Time Clock Work ");
      setSize(480, 320);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      
      //aligne la fenêtre au milieu de l'ecran
      setLocationRelativeTo(null); 
      
      
      // Pannel qui contiendra les objets de la fenêtre 
      JPanel contentPane = new JPanel();
      contentPane.setLayout(null);
      this.setContentPane(contentPane);
		
      
      // Label contenant le jour d'aujourd'hui
      lblDate = new JLabel("Today's date");
      lblDate.setBounds(150,10, 200, 70);
      lblDate.setFont(new Font("Verdana", 1 ,30));
      this.getContentPane().add(lblDate);
      
      
      // Label contenant l'heure exacte
      lblHour = new JLabel("Hour undefinied");
      lblHour.setBounds(150,40, 200, 70);
      lblHour.setFont(new Font("Verdana", 1 ,30));
      this.getContentPane().add(lblHour);

      
      // Label contenant l'heure arrondi au quart d'h
      lblHourRounded = new JLabel("Hour undefinied");
      lblHourRounded.setBounds(150, 70, 200, 100);
      lblHourRounded.setFont(new Font("Verdana", 1, 20));
      this.getContentPane().add(lblHourRounded);

      
      // Bouton de pointage 
      JButton btnPointageIN = new JButton("In/Out");
      btnPointageIN.setBounds(300,150, 80, 30);
      btnPointageIN.setBackground(new Color(233, 150, 122));
      btnPointageIN.addActionListener(this);
      btnPointageIN.setActionCommand("point");
      this.getContentPane().add(btnPointageIN);
      
      
      // Timer qui rafraichit l'heure
      Timer TimerDate = new Timer(1000, this);
      TimerDate.setRepeats(true);
      TimerDate.setCoalesce(true);
      TimerDate.setInitialDelay(0);
      TimerDate.start();
      setVisible(true);
      
      
      // Empêche l'auto Focus après que l'heure rafraichit
      setAutoRequestFocus(false);     
      
      // Événement de la fenêtre
      addWindowListener(new WindowAdapter() {
    	  
    	 /**
    	   * Ouvre un serveur lors de l'ouverture de la fenêtre
    	   * @param we - Évenement liée à la fenetre
    	   */
          @Override
          public void windowOpened(WindowEvent we) {
        	  // Serveur pour connexion TCP
        	  Server s = new Server();
        	  new Thread(s).start();
        	  listEmployee = s.getList();  
          }
          
      });
      

      // Liste déroulante permettant à l'utilisateur de choisir son employé
      comboBoxEmployee = new JComboBox<ConvertEmployee>();
	  comboBoxEmployee.setBounds(130, 150, 150, 30);
	  comboBoxEmployee.setVisible(false);
	  getContentPane().add(comboBoxEmployee);
	  
	  
	  // Permet d'attendre un temps avant la mise a jour de la liste déroulante
	  // qui est envoyé par l'application, pour éviter des erreurs 
	  new java.util.Timer().schedule( 
		     new java.util.TimerTask() {
		    	 
	            @Override
		        public void run() {
		          	fillComboBox();
		        }
	            
		     }, 
		     500
	  );
      
  }
  
  
  
  
    //------ METHODES ------//
  
	/**
	   * Remplit la liste déroulante des employés et fait apparaitre la liste
	   */
    public void fillComboBox() {
	    for(int i=0; i<this.listEmployee.size();i++ ) {
	    	comboBoxEmployee.addItem(this.listEmployee.get(i));
		}
	    comboBoxEmployee.setVisible(true);
    }
  
    
	/**
	   *  Méthode éxecute à chaque action sur un objet de la librairie swing
	   *  @param evt - Évenement d'action  
	   *  
	   */
	public void actionPerformed(ActionEvent evt) {

		if (evt.getSource() instanceof Timer) {
			
			DateHour Date = new DateHour();
			
			// affiche la date et l'heure
			lblDate.setText("" + Date.getTodayDate());
			lblHour.setText("" + Date.getHour()); 
			
			// affiche l'heure arrondie
			LocalTime lastQuarter = Date.RoundedToQuarter();
			lblHourRounded.setText("Let's say "+ lastQuarter);
			
			// Cherche la date de l'heure arrondie et l'initialise au pointage
			LocalDateTime dateTime = lastQuarter.atDate(Date.getTodayDate());
			markTime = dateTime;
		
			//Affichage du message en bas de fenetre
			//0 : pas de msg;
			//10 : un msg donc on commence le decompte
			if (cptTimer >= 10) {
				cptTimer++;
				if (cptTimer >= 15) {
					cptTimer = 0;
					lblError.setText("");
				}
			}
		}
	
		
		if (evt.getSource() instanceof JButton) {
			
			// si on clique sur IN/OUT
			if (evt.getActionCommand() == "point") {
				
				// On prend l'employé selectionné
				ConvertEmployee selected = (ConvertEmployee) this.comboBoxEmployee.getSelectedItem();
				
				// On prend la date et l'heure 
				// que l'on ajoute a une liste de date
				ListMarkings m = new ListMarkings();
				m.addMarking(markTime);
				
				// Ajoute à la liste des pointages
		        markList.put(selected,m);

		        // Lance le Client avec la liste des pointages
				new Thread(new ClientSendMarking(markList)).start();
				
			}
		}
			
		setVisible(true);
		
	} 

} 

package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import Controler.ConvertListEmployee;
import Controler.EmployeeList;
import Model.Employee;
import Model.EmployeeListTable;
import Model.ListMarkings;
import Model.MarkingListTable;
import tcp.Client;
import tcp.ServerSendMarking;
import Model.Company;
import Model.ConvertEmployee;

public class Application extends JFrame implements ActionListener{
	
	//------ ATTRIBUTS ------//
	Company company; 
	
	JTable tblEmployee; //jtable contenant tous les Employee de la company

	JButton btnUpdate; 
	JButton btnAddEmp; //permet d'accéder a la page permettant d'ajouter un emp
	JButton btnAddEmp2; //appelle la méthode d'ajout de l'emp
	JButton btnDelEmp; //permet d'accéder a la page permettant de supprimer un emp
	JButton btnDelEmp2; //appelle la méthode de suppression de l'emp
	JButton btnPrintMarking; //affiche une jtable avec les pointages d'un emp choisit dans une liste déroulante
	JButton btnMarkUpdate;
	JButton btnOpenServer; //permet d'ouvrir le serveur
	JButton btnReturn;
	
	JTextField txtbName = new JTextField("Name");
	JTextField txtbFname = new JTextField("First Name");
	
	JComboBox<String> cbxDepList = new JComboBox<String>();
	JComboBox<String> cbxEmpList = new JComboBox<String>();
	JComboBox<String> cbxEmpList_Marking = new JComboBox<String>();

	//hashmap qui permet de stocker les informations recu de la pointeuse
	HashMap<ConvertEmployee,ListMarkings> markList = new HashMap<ConvertEmployee,ListMarkings>(); //  Liste d'employ�e et datetime de la pointeuse pour utilisation dans l'application
	
	
	//------ METHODES ------//
	/**
	 * Gère les actions générés par les èvenements : 
	 * appui sur bouton, changement de menu... 
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		// TO DO Auto-generated method stub
		removeCP(); //remove content pane & repaint
		
		if (evt.getActionCommand() == "btnPrintMarking") {
			int i = 0;
			String firstLetter = "";
			String stringUID = "";
			while (!firstLetter.equalsIgnoreCase(".")) {
				stringUID = stringUID+firstLetter;
				firstLetter = String.valueOf(((String)cbxEmpList_Marking.getSelectedItem()).charAt(i));
				i++;
			}
        	int uid = Integer.parseInt(stringUID);
        	Employee emp = null;
        	for (int CptDep = 0; CptDep < company.getSizeDepartmentList(); CptDep++) {
        		for (int CptEmp = 0; CptEmp < company.getDepartment(CptDep).getSizeEmployeeList(); CptEmp++) {
        			if (uid == company.getDepartment(CptDep).getEmployeeAt(CptEmp).getUserID()) {
                    	emp = company.getDepartment(CptDep).getEmployeeAt(CptEmp);
        			}
        		}
        	}
        	
        	JLabel lblName = new JLabel("   	Liste de tous les pointages de  :  " + emp.getFirstName()+"  "+emp.getName());
        	lblName.setBounds(20, 20, 400, 20);
        	getContentPane().add(lblName);
        	getContentPane().add(btnReturn);
        	getContentPane().add(this.createJTableMarking(emp));
			JScrollPane spEmp = new JScrollPane(tblEmployee);
		    getContentPane().add(spEmp);
		    
		
			}
		
		if (evt.getSource() instanceof JMenuItem) {
			
			//action lors du clic sur le menu management
			if (evt.getActionCommand() == "menuManagement") { 	
				getContentPane().add(this.createJTableEmployee());
				JScrollPane spEmployee = new JScrollPane(tblEmployee, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			    getContentPane().add(spEmployee);
				getContentPane().add(btnUpdate);
			}
			
			if (evt.getActionCommand() == "menuEdit") {
				//affichage des boutons pour ajouter/supp les emp
				getContentPane().add(btnAddEmp);
				getContentPane().add(btnDelEmp);
			}
			
			if (evt.getActionCommand() == "menuInput") {
				getContentPane().add(btnOpenServer);
			}
			
			if (evt.getActionCommand() == "menuHistory") {
				getContentPane().add(fillCbxEmployee(cbxEmpList_Marking));
				getContentPane().add(btnPrintMarking);
				getContentPane().add(btnMarkUpdate);
				
			}
		}
		
		
		if (evt.getSource() instanceof JButton) {
			
			// --- Bouton de suppression d'un employ --- //
			if (evt.getActionCommand() == "btnDelEmp") {
				//affiche la liste deroulante des emp 
				getContentPane().add(fillCbxEmployee(cbxEmpList));
				getContentPane().add(btnDelEmp2);
			}
			//supprime l'employ et affiche un msg 
			if (evt.getActionCommand() == "btnDelEmp2") {
				
				//on recupere le 1er caractere du string (qui contient l'id)
            	int i = 0;
    			String firstLetter = "";
    			String stringUID = "";
    			while (!firstLetter.equalsIgnoreCase(".")) {
    				stringUID = stringUID+firstLetter;
    				firstLetter = String.valueOf(((String)cbxEmpList.getSelectedItem()).charAt(i));
    				i++;
    			}
            	int uid = Integer.parseInt(stringUID);
            	
            	//on cherche dans tous les departements : A quel employ l'id appartient
            	for (int CptDep = 0; CptDep < company.getSizeDepartmentList(); CptDep++) {
            		for (int CptEmp = 0; CptEmp < company.getDepartment(CptDep).getSizeEmployeeList(); CptEmp++) {
            			if (uid == company.getDepartment(CptDep).getEmployeeAt(CptEmp).getUserID()) {
                        	company.getDepartment(CptDep).delEmployeeFromList(CptEmp);
            			}
            		}
            	}

				JLabel lblMsg = new JLabel("Employee removed with success !");
				getContentPane().add(lblMsg);
			}
			
			// --- Bouton d'ajout d'un employ --- //
			if (evt.getActionCommand() == "btnAddEmp") {
				//affiche les text box pour ajouter l'employ
				txtbName.setText("Name");
				txtbFname.setText("First Name");
				cbxDepList.removeAllItems();
				for (int Cpt = 0; Cpt<company.getSizeDepartmentList(); Cpt++) {
					cbxDepList.addItem(company.getDepartment(Cpt).getNameDepartment());
				}
				getContentPane().add(txtbName);
				getContentPane().add(txtbFname);
				getContentPane().add(cbxDepList);
				getContentPane().add(btnAddEmp2);
			}
			//ajoute l'employ et affiche un msg 
			if (evt.getActionCommand() == "btnAddEmp2") {
				Employee newEmployee = new Employee(txtbName.getText(),
													txtbFname.getText(),
													company.getDepartment(cbxDepList.getSelectedIndex())); 
				company.getDepartment(cbxDepList.getSelectedIndex()).addEmployeeToList(newEmployee);
				JLabel lblMsg = new JLabel("Employ ajout avec succs !");
				getContentPane().add(lblMsg);
			}
			
			// --- Bouton de maj de la liste d'employs de la table --- //
			if (evt.getActionCommand() == "btnUpdate") {
				getContentPane().add(this.createJTableEmployee());
				JScrollPane spEmployee = new JScrollPane(tblEmployee, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			    getContentPane().add(spEmployee);
				getContentPane().add(btnUpdate);
			}
			
			if (evt.getActionCommand() == "server"){
		    	System.out.println("The server is open");
		    	
				ServerSendMarking s = new ServerSendMarking();
		    	new Thread(s).start();
		    	markList = s.getList();
		    	update();

		    	btnOpenServer.setVisible(false);
		    	
		    	getContentPane().add(fillCbxEmployee(cbxEmpList_Marking));
				getContentPane().add(btnPrintMarking);
				getContentPane().add(btnMarkUpdate);
			}
			
			if (evt.getActionCommand() == "update"){
				update();
				getContentPane().add(fillCbxEmployee(cbxEmpList_Marking));
				getContentPane().add(btnPrintMarking);
				getContentPane().add(btnMarkUpdate);
			}
			
			if (evt.getActionCommand() == "return"){
				getContentPane().add(fillCbxEmployee(cbxEmpList_Marking));
				getContentPane().add(btnPrintMarking);
				getContentPane().add(btnMarkUpdate);
			}
		}
		
		setVisible(true);
	}
	
	/**
	 * Constructeur par défaut 
	 */
	public Application() {
    	setTitle("Application");
	    setSize(720, 480);

	    // Pannel qui contiendra les objets de la fenetre (bouton, label) 
	    JPanel contentPane = new JPanel();
	    this.setContentPane(contentPane);
	    setJMenuBar(this.createMenuBar());
	    
	    //bouton pour mettre a jour la table des employs
        btnUpdate = new JButton("Update");
        btnUpdate.addActionListener(this);
        btnUpdate.setActionCommand("btnUpdate");
        
        //bouton pour acceder au menu d'ajout d'un emp
        btnAddEmp = new JButton("Add Employee");
        btnAddEmp.addActionListener(this);
        btnAddEmp.setActionCommand("btnAddEmp");
        
        //bouton qui ajoute l'emp
        btnAddEmp2 = new JButton("Add Employee");
        btnAddEmp2.addActionListener(this);
        btnAddEmp2.setActionCommand("btnAddEmp2");
        
        //bouton pour accéder au menu de suppression d'un emp
        btnDelEmp = new JButton("Delete Employee");
        btnDelEmp.addActionListener(this);
        btnDelEmp.setActionCommand("btnDelEmp");
        
        //bouton qui supprime l'emp
        btnDelEmp2 = new JButton("Delete Employee");
        btnDelEmp2.addActionListener(this);
        btnDelEmp2.setActionCommand("btnDelEmp2");
        
        //Bouton qui affiche les pointages de l'emp choisit dans la liste déroulante
        btnPrintMarking = new JButton("Print marking");
        btnPrintMarking.addActionListener(this);
        btnPrintMarking.setActionCommand("btnPrintMarking");
        
        //bouton qui ouvre un serveur
	    btnOpenServer = new JButton("Open server");
	    btnOpenServer.addActionListener(this);
	    btnOpenServer.setActionCommand("server");
	    getContentPane().add(btnOpenServer);
		  
	    //bouton retour
	    btnReturn = new JButton("Return");
	    btnReturn.addActionListener(this);
	    btnReturn.setActionCommand("return");
	     
	    //bouton qui update les pointages 
	    btnMarkUpdate = new JButton("Update");
	    btnMarkUpdate.addActionListener(this);
	    btnMarkUpdate.setActionCommand("update");
        

	    addWindowListener(new WindowAdapter() {
            /**
             * Action lors de l'ouverture de la fen�tre
             * @param we l'evenement g�n�r� lors de l'ouverture de la fenetre
             */
	    	@Override
            public void windowOpened(WindowEvent we) {         	
	    		deserialize();
            	
    			//Envoie liste des employ�es vers pointeuse 
            	ConvertListEmployee convertedList = new ConvertListEmployee();
            	
            	for(int i=0;i<company.getSizeDepartmentList();i++) {
            		convertedList.addList(company.getDepartment(i).getListEmployee());
            	}
   
            	new Thread(new Client(convertedList.getNewListEmployee())).run();
            	 
            }
	    	
	    	/**
             * Action lors de la fermeture de la fen�tre
             * @param we l'evenement généré lors de l'ouverture de la fenetre
             */
	  		@Override
    		public void windowClosing (WindowEvent we) {
	  			//serialisation de company à l'ouverture de l'application
        		try {
        			FileOutputStream fos = new FileOutputStream("companyfileser");
        			ObjectOutputStream oos = new ObjectOutputStream(fos);
        			oos.writeObject(company);
        			System.out.println("Company file saved !");
        			oos.close();
        	
        		} catch (Exception e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
				
			}

		});
    	setVisible(true);
    } //fin du constructeur
	
	/**
	 * Met a jour la liste des pointages des employés avec les données de la pointeuse
	 */
    private void update() {
    	for(Map.Entry<ConvertEmployee,ListMarkings> entry : this.markList.entrySet()) {
			ConvertEmployee key = entry.getKey();
			ListMarkings value = entry.getValue();
			findEmployee(key).setListMarking(value);
		}
	}
    
    /**
     * Retourne l'Employee qui correspond au convertEmployee en paramètre
     * @param paramEmployee Le convertEmployee a partir duquel on veut retrouver un Employee
     * @return (Employee) - L'employee (si il a été trouvé), sinon null
     */
    private Employee findEmployee(ConvertEmployee paramEmployee) {
    	for (int CptDep = 0; CptDep < company.getSizeDepartmentList(); CptDep++) {
    		for (int CptEmp = 0; CptEmp < company.getDepartment(CptDep).getSizeEmployeeList(); CptEmp++) {
    			if (paramEmployee.getUserID() == company.getDepartment(CptDep).getEmployeeAt(CptEmp).getUserID()) {
    				return company.getDepartment(CptDep).getEmployeeAt(CptEmp);
    			}
    		}
    	}
    	return null;
	}
    
    /**
     *  Methode permettant de supprimer tous les éléments du contentPane 
     */
    private void removeCP() {
		this.getContentPane().removeAll();
		this.getContentPane().revalidate();
		this.getContentPane().repaint();
    }
    
    /**
     * Methode permettant de remplir une liste déroulante avec tous les emp
     */
    public JComboBox<String> fillCbxEmployee(JComboBox<String> cbxEmp) {
    	cbxEmp.removeAllItems();
        for (int CptDep = 0; CptDep < company.getSizeDepartmentList(); CptDep++) {
        	int sizeEmpList = company.getDepartment(CptDep).getSizeEmployeeList();
        	for (int CptEmp = 0; CptEmp < sizeEmpList; CptEmp++) {
        		int uid = company.getDepartment(CptDep).getEmployeeAt(CptEmp).getUserID();
        		String fname = company.getDepartment(CptDep).getEmployeeAt(CptEmp).getFirstName();
        		String name = company.getDepartment(CptDep).getEmployeeAt(CptEmp).getName();
        		String name_uid = uid+". "+name+" "+fname;
        		cbxEmp.addItem(name_uid);
        	}
        }
        return cbxEmp;
    }
    
    /**
     *  Méthode de construction et de mise a jour de la JTable qui contient la liste des employés 
     */
    private JTable createJTableEmployee() {
		EmployeeList list1 = new EmployeeList(this.company);
		list1.update();	
		EmployeeListTable empTable = new EmployeeListTable(list1);		
		tblEmployee = new JTable(empTable);
	    return tblEmployee;
    }
    
    /**
     *  Méthode de construction et de mise a jour de la JTable
     *  qui contient les pointages d'un Employee passé en paramètre
     *  @param paramEmployee L'Employee duquel on souhaite afficher la liste des pointages 
     */
    private JTable createJTableMarking(Employee paramEmployee) {
		ListMarkings list = paramEmployee.getListMarking();
		MarkingListTable markTable = new MarkingListTable(list);
		tblEmployee = new JTable(markTable);
		tblEmployee.setRowHeight(30);
	
	    return tblEmployee;
    }

	/**
	 *  Methode de construction de la barre de menu 
	 */
	private JMenuBar createMenuBar() {
	
	    // La barre de menu 
	    JMenuBar menuBar = new JMenuBar();
	    
	    // Définition du menu "Check IN/OUT" et de son contenu
	    JMenu menuCheck = new JMenu("Markings");
	    
	    //le menu history permet d'afficher tous les pointages
	    JMenuItem menuHistory = new JMenuItem("History");
	    menuCheck.add(menuHistory);
	    menuHistory.setActionCommand("menuHistory");
	    menuHistory.addActionListener(this);
	    
	    //le menu input permet de gérer l'update des pointages et l'ouverture du serveur
	    JMenuItem menuInput = new JMenuItem("Input");
	    menuInput.addActionListener(this);
	    menuInput.setActionCommand("menuInput");
	    menuCheck.add(menuInput);
	    
	    menuBar.add(menuCheck);

	    // Définition du menu "Staff" et de son contenu
	    JMenu menuStaff = new JMenu( "Staff" );
	    
	    // le menu managment affiche la liste des Employee
	    JMenuItem menuManagement = new JMenuItem("Management");
	    menuStaff.add(menuManagement);
	    menuManagement.addActionListener(this);
	    menuManagement.setActionCommand("menuManagement");
        
	    //le menu edit permet de gérer la liste des Employee
	    JMenuItem menuEdit = new JMenuItem("Edit");
	    menuEdit.addActionListener(this);
	    menuEdit.setActionCommand("menuEdit");
	    menuStaff.add(menuEdit);
	    
	    menuBar.add(menuStaff);
	    
	    return menuBar;
	}
	
	/**
	 * Deserialise l'objet company dans le fichier "companyfileser" 
	 * et le stocke dans l'objet company dans les attributs de la classe 
	 */
	public void deserialize() {
		//deserialisation a l'ouverture de l'application 
    	try {
			FileInputStream file = new FileInputStream("companyfileser");
			ObjectInputStream ois = new ObjectInputStream(file);
			Company c = (Company) ois.readObject();
			this.setCompany(c);
			System.out.println("Opening company file ...");
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pour définir l'attribut company de la classe avec un objet company prit en paramètre
	 * @param paramCompany La company qui permet d'instancier l'attribut
	 */
	private void setCompany(Company paramCompany) {
		this.company = paramCompany;
	}

}

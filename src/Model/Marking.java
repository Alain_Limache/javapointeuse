package Model;

import java.time.LocalDateTime;

/**
 *  Classe qui stocke 
 *  - un employé
 *  - l'heure et date du pointage
 */
public class Marking {
	
	//------ATTRIBUTS------// 
	private Employee employee;       
	private LocalDateTime dateTime;  
	
	
	//------CONSTRUCTEURS------//
	/**
	  * Instancie un nouvel objet Marking 
	  * avec les arguments passés en parametres du constructeur.
	  * @param paramEmployee Un employé à instancier
	  * @param paramTime L'heure et la date à instancier
	  */
	public Marking(Employee paramEmployee, LocalDateTime paramTime ) {
		this.setEmployee(paramEmployee);
		this.setDateTime(paramTime);
	}
	
	//------METHODES------//
	/**
	 * Renvoie l'employé à qui correspond le pointage
	 * @return (Employee) employee - l'employé
	 */
	public Employee getEmployee() {
		return this.employee;
	}
	
	
	/**
	 * Definit l'employé qui fait le pointage
	 * @param paramEmployee l'employé
	 */
	public void setEmployee(Employee paramEmployee) {
		this.employee = paramEmployee;
	}
	
	
	/**
	 * Renvoie l'heure et la date du pointage
	 * @return (LocalDateTime) dateTime - l'heure et date du pointage
	 */
	public LocalDateTime getDateTime() {
		return this.dateTime;
	}
	
	
	/**
	 * Definit l'heure et la date du pointage
	 * @param paramTime l'heure et date du pointage
	 */
	public void setDateTime(LocalDateTime paramTime) {
		this.dateTime = paramTime;
	}
}

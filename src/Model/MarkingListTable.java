package Model;

import java.time.LocalDateTime;

import javax.swing.table.AbstractTableModel;

import Controler.EmployeeList;
import java.time.*;

/**
 *  Classe qui stocke 
 *  - une liste d'EmployeeList
 */

public class MarkingListTable extends AbstractTableModel  {
	
	 //------ ATTRIBUT ------//
	ListMarkings MarkingList;
	
	//------ CONSTRUCTEUR ------//
	/**
	 * Constructeur, 
	 * Instancie un nouvel objet MarkingListTable
	 * @param listEmployee
	 */
	public MarkingListTable(ListMarkings MarkingList) {
		this.MarkingList = MarkingList;
	}
	
	 //------ METHODES ------//

	/**
	* Retourne le nom des colonnes 
	* @param col l'index du nom de la colonne à retourner
	* @return (String) - Le nom de la colonne
	*/
	@Override
	public String getColumnName(int col) {
		String[] columnNames = {"Heure",
				"Date (DD/MM/YYYY)"};
		return columnNames[col];
	}
	
	/**
	 * @return (int) - Le nombre de ligne du tableau
	 */
	public int getRowCount() {
		return MarkingList.getSize();
	}

	/**
	 * @return (int) Le nombre de colonne du tableau
	 */
	public int getColumnCount() {
		//heure, date. 
		return 2;
	}

	/**
	 * Méthode qui retourne l'objet en positions données en paramètre : 
	 * @param rowIndex
	 * @param columnIndex
	 * @return (Object) - l'objet dans cette position
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		String hour = this.MarkingList.getAt(rowIndex).getHour() + ":" + this.MarkingList.getAt(rowIndex).getMinute();
		String date = this.MarkingList.getAt(rowIndex).getDayOfMonth() + "/" +  this.MarkingList.getAt(rowIndex).getMonthValue()  + "/" +  this.MarkingList.getAt(rowIndex).getYear();
		switch(columnIndex) 
		{
			case 0: return hour;
			case 1: return date;
			default : return "undefine";
		}
	}
		
	   /**
	   * Retourne la classe à laquelle appartient les objets dans la colonne donnée en paramètre
	   * @param columnIndex1 Le niméro de colonne
	   * @return (Class) - La classe de la colonne 
	   */
		public Class<? extends Object>getColumnClass(int columnIndex1) {
			return getValueAt(0,columnIndex1).getClass();	
		}
}
	

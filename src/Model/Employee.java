package Model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import Model.Department;

/**
 *  Classe qui stocke 
 *  - identifiant unique de l'employée qui s'incrémente automatiquement
 *  - son nom
 *  - son prénom
 *  - ses heures suplémentaires
 *  - son département
 *  - son planning
 *  - sa liste de pointage
 */

public class Employee implements Serializable{

	//------ATTRIBUTS------// 
	private static final AtomicInteger count = new AtomicInteger(0); 
	private final int userID;					
	private String name;						
	private String firstName;					
	private LocalTime extraTime;				
	private Department department;				
	private Planning planning;					
	private ListMarkings listMarking;     
	
	//------CONSTRUCTEURS------//

	/**
	  * Instancie un nouvel objet Employee 
	  * avec les arguments passés en parametres du constructeur.
	  * @param paramName Nom de l'employé à instancier
	  * @param paramFirstName Prenom de l'employé à instancier
	  * @param paramDep departement de l'employé à instancier
	  */
	public Employee(String paramName, String paramFirstName, Department paramDep) {
		this.userID = count.incrementAndGet();
		this.department = paramDep;
		this.setName(paramName);
		this.setFirstName(paramFirstName);
	    this.extraTime = LocalTime.of(0, 0);
	    this.department = paramDep;
	    this.listMarking = new ListMarkings();
	    this.planning = new Planning(); 
	    this.planning.PlanningByDefault();
	}

	/**
	  * Instancie un nouvel objet Employee avec un planning
	  * avec les arguments passés en parametres du constructeur.
	  * @param paramName Nom de l'employé à instancier
	  * @param paramFirstName Prenom de l'employé à instancier
	  * @param paramPlanning planning de l'employé à instancier
	  */
	public Employee(String paramName, String paramFirstName, Department paramDep, Planning paramPlanning) {
		this.userID = count.incrementAndGet();
		this.department = paramDep;
		this.setName(paramName);
		this.setFirstName(paramFirstName);
	    this.extraTime = LocalTime.of(0, 0);
	    this.department = paramDep;
	    this.listMarking = new ListMarkings();
	    this.setPlanning(paramPlanning);
	}

	
	//------METHODES------//
	/**
	 * Renvoie l'ID de l'employe 
	 * @return (int) userID - l'id de l'employé
	 */
	public int getUserID() {
		return this.userID;
	}
	
	
	/**
	 * Renvoie le nom de l'employe
	 * @return (String) name - le nom de l'employé
	 */
	public String getName() {
		return this.name;
	}
	
	
	/**
	 * Renvoie le prénom de l'employé
	 * @return (String) firstName - le prénom de l'employé
	 */
	public String getFirstName() {
		return this.firstName;
	}
	
	
	/**
	 * Définit le nom de l'employé
	 * @param paramName - le nom de l'employé
	 */
	public void setName(String paramName) {
		this.name = paramName;
	}
	
	
	/**
	 * Définit le prénom de l'employé
	 * @param paramFirstName - le prénom de l'employé
	 */
	public void setFirstName(String paramFirstName) {
		this.firstName = paramFirstName;
	}
	
	
	/**
	 * Renvoie la liste des pointages de l'employé
	 * @return (ArrayList<Marking>) listMarking - la liste des pointages de l'employé
	 */
	public ListMarkings getListMarking() {
		return this.listMarking;
	}
	
	/**
	 * Définit la liste des pointages de l'employé
	 * @param listMarking - la liste des pointages de l'employé
	 */
	public void setListMarking(ListMarkings paramList) {
		boolean isIn;
		for(int i=0 ; i < paramList.getMarkings().size(); i++) {
			isIn = false;
			
			for(int j=0 ; j < this.listMarking.getSize(); j++) {
				if(this.listMarking.getAt(j).getHour() == paramList.getMarkings().get(i).getHour() && this.listMarking.getAt(j).getMinute() == paramList.getMarkings().get(i).getMinute() &&
				   this.listMarking.getAt(j).getSecond() == paramList.getMarkings().get(i).getSecond() && this.listMarking.getAt(j).getDayOfMonth() == paramList.getMarkings().get(i).getDayOfMonth() &&
				   this.listMarking.getAt(j).getMonth() == paramList.getMarkings().get(i).getMonth() && this.listMarking.getAt(j).getYear() == paramList.getMarkings().get(i).getYear() ) {
					isIn = true;
				}	
			}
			
			if (isIn == false) {
				this.listMarking.addMarking(paramList.getMarkings().get(i));
			}
		}
	}
	
	/**
	 * Définit le département de l'employé
	 * @param paramDep - le departement de l'employé
	 */
	public void setDepartement(Department paramDep) {
		this.department = paramDep;
	}
	
	
	/**
	 * Renvoie le département de l'employé
	 * @return (Department) department - le departement de l'employé
	 */
	public Department getDepartement() {
		return this.department;
	}
	
	
	/**
	 * Définit le planning de l'employé
	 * @param paramPlanning - le planning de l'employé 
	 */
	public void setPlanning(Planning paramPlanning) {
		this.planning = paramPlanning;
	}
	
	
	/**
	 * Renvoie le planning de l'employé
	 * @return (Planning) planning - le planning de l'employé
	 */
	public Planning getPlanning() {
		return this.planning;
	}
	
	
	/**
	 * Renvoie le nombre d'heure(s) supplementaire(s) de l'employé
	 * @return (int) extraTime - le nombre d'heure sup de l'employé
	 */
	public LocalTime getExtraTime() {
	  return this.extraTime;
	}
	
	
	/**
	 * Definit le nombre d'heure(s) supplementaire(s) de l'employé
	 * @param time - Le nombre d'heure(s) que l'on veut rajouter
	 */
	public void setExtraTime(LocalTime time) {
		this.extraTime = time;
	}
	
	
	/**
	 * Modifie le nombre d'heure(s) supplementaire(s) de l'employé (ajout)
	 * @param minutesToAdd - Le nombre de minutes que l'on veut rajouter
	 */
	public void addExtraTime(long minutesToAdd) {
		this.extraTime = this.extraTime.plusMinutes(minutesToAdd);
	}	
  
}


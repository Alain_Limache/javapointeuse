package Model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 *  Classe qui stocke 
 *  - la liste des pointages
 */
 
public class ListMarkings implements Serializable{

	//------ ATTRIBUTS ------//
	
	private ArrayList<LocalDateTime> markings;
	
		//------ Constructeurs ------//

	/**
	  * Instancie un nouvel objet ListMarkings 
	  * avec l' argument passé en parametre du constructeur.
	  * @param paramMarkings 
	  */
	public ListMarkings(ArrayList<LocalDateTime> paramMarkings ) {
		this.markings = new ArrayList<LocalDateTime>();
		for(int i=0 ; i < paramMarkings.size(); i++) {
			this.markings.add(paramMarkings.get(i));
		}
	}
	
	/**
	* Instancie un nouvel objet ListMarkings 
	*/
	public ListMarkings( ) {
		this.markings = new ArrayList<LocalDateTime>();
	}
	
		//------ METHODES ------//

	/**
	 * Renvoie les pointages
	 * @return (LocalDateTime) markings - pointages
	 */
	public ArrayList<LocalDateTime> getMarkings() {
		return this.markings;
	}

	/**
	 * vide la liste des pointages
	 */	
	public void resetList() {
		this.markings.clear();
	}
	
	/**
	* Retourne la taille de la liste de pointages
	* @return (int) - Taille de la liste de pointages
	*/
	public int getSize() {
		return this.markings.size();
	}
	
	/**
	* Retourne la date et l'heure de pointage à la position demande
	* @param i position a laquelle on souhaite obtenir ces informations
	* @return (LocalDateTime) - date et heure de pointage
	*/
	public LocalDateTime getAt(int i) {
		return this.markings.get(i);
	}
	
	
	/**
	* @return (boolean) - la liste est elle pleine?
	*/
	public boolean isEmpty() {
		return this.markings.isEmpty();
	}
	
	/**
	* Ajouter une date et l'heure de pointage 
	* @param paramTime l'heure et date de ppointage à ajouter
	*/
	public void addMarking(LocalDateTime paramTime) {
		this.markings.add(paramTime);
	}
	
	/**
	* Affichage des pointages
	*/
	public String toString() {
		String s = "";
		for(int i=0 ; i < this.markings.size(); i++) {
			s = s + this.markings.get(i).getHour() + ":" + this.markings.get(i).getMinute() + " le " + this.markings.get(i).getDayOfMonth() + "/" +  this.markings.get(i).getMonthValue()  + "/" +  this.markings.get(i).getYear() + "\n";
		}
		return s;	
	}
	


}

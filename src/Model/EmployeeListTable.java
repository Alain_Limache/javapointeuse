package Model;

import java.util.ArrayList;
import Controler.EmployeeList;
import javax.swing.table.AbstractTableModel;

/**
 *  Classe qui stocke 
 *  - EmployeeList une liste d'employée 
 */

public class EmployeeListTable extends AbstractTableModel {

	//------ ATTRIBUT ------//
	EmployeeList listEmployee;
	
	//------ CONSTRUCTEUR ------//
	/**
	 * Constructeur
	 * Instancie un nouvel objet EmployeeListTable
	 * @param listEmployee
	 */
	public EmployeeListTable(EmployeeList listEmployee) {
		this.listEmployee = listEmployee;
	}
	
	//------ METHODES ------//
	
	/**
	* Avoir le nom des colonnes 
	* @param col l'index du nom de la colonne à retourner
	* @return (String) - Le nom de la colonne
	*/
	@Override
	public String getColumnName(int col) {
		String[] columnNames = {"User ID",
				"First Name",
                "Last Name",
                "Department"};
	    return columnNames[col];
	}
	
	/**
	 * @return (int) Le nombre de ligne du tableau
	 */
	public int getRowCount() {
		// TODO Auto-generated method stub
		return listEmployee.getSize();
	}

	/**
	 * @return (int) Le nombre de colonne du tableau
	 */
	public int getColumnCount() {
		// TODO Auto-generated method stub
		//id, nom, prenom, dep. 
		return 4;
	}

	/**
	* Méthode qui retourne l'objet en positions données en paramètre : 
	* @param rowIndex
	* @param columnIndex
	* @return (Object) - l'objet dans cette position
	*/
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Employee emp = listEmployee.getEmployee(rowIndex);
		switch(columnIndex) 
		{
			case 0: return emp.getUserID();
			case 1: return emp.getName();
			case 2: return emp.getFirstName();
			case 3: return emp.getDepartement().getNameDepartment();
			default : return "undefine";
		}
	}
	
	 /**
	* Retourne la classe à laquelle appartient les objets dans la colonne donnée en paramètre
	* @param columnIndex Le niméro de colonne
	* @return (Class) - La classe de la colonne 
	*/
	@Override
	public Class<? extends Object>getColumnClass(int columnIndex) {
		return getValueAt(0,columnIndex).getClass();
		
	}
	
	
	
	
	
}

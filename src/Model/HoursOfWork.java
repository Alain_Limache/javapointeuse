package Model;

import java.io.Serializable;
import java.time.LocalTime;

/**
 *  Classe qui stocke 
 *  - Heure de début de travail
 *  - Heure de fin de travail 
 */

public class HoursOfWork implements Serializable {
	
	//------ATTRIBUTS------// 
	private LocalTime begin; 
	private LocalTime ending; 
	
	//------CONSTRUCTEURS------//
	
	/**
	  * Instancie un nouvel objet HoursOfWork 
	  * avec les arguments passés en parametres du constructeur.
	  * @param start Heure de début de travail à instancier
	  * @param end Heure de fin de travail à instancier
	  */
	public HoursOfWork(LocalTime start, LocalTime end) {
		this.setBeginTime(start);
		this.setEndingTime(end);
	}
	
	
	//------METHODES------//
	/**
	 * Renvoie l'heure de début
	 * @return (LocalTime) begin - Heure de début de travail
	 */
	public LocalTime getBeginTime() {
		return this.begin;
	}
	
	
	/**
	 * Renvoie l'heure de fin
	 * @return (LocalTime) ending - Heure de fin de travail
	 */
	public LocalTime getEndingTime() {
		return this.ending;
	}
	
	
	/**
	 * Définit l'heure de début
	 * @param paramTime - Heure de début  de travail
	 */
	public void setBeginTime(LocalTime paramTime) {
		this.begin = paramTime;
	}
	
	
	/**
	 * Définit l'heure de fin
	 * @param paramTime - Heure de fin de travail
	 */
	public void setEndingTime(LocalTime paramTime) {
		this.ending = paramTime;
	}
	

}

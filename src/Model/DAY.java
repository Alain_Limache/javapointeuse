package Model;

/**
 * Enumération des jours de la semaine possible
 */
public enum DAY {
	MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

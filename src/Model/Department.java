package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *  Classe qui stocke 
 *  - le nom du département
 *  - la liste des employées de ce département  
 */

public class Department implements Serializable {
	
	  //------ ATTRIBUTS ------//
	  private String nameDep; 
	  private ArrayList<Employee> listEmployee; 
	  
	  
	  //------ CONSTRUCTEURS ------//
	  /**
	   * Contructeur par défaut 
	   * Instancie un nouvel objet Department (sans nom)
	   */
	  public Department() 
	  {
		  listEmployee = new ArrayList<Employee>();  
	  }
	  
	  /**
	   * Instancie un nouvel objet Department 
	   * @param paramNameDep Le nom du departement
	   */
	  public Department(String paramNameDep) {
		  listEmployee = new ArrayList<Employee>(); 
		  this.setNameDepartment(paramNameDep);
	  }
	  
	  
	  //------ METHODES ------//
	  /**
	   * Retourne le nom du departement
	   * @return (String) - Le nom du departement
	   */
	  public String getNameDepartment() {
		  return this.nameDep;
	  }
	  
	  /**
	   * Modifie le nom du departement
	   * @param paramNameDep Nouveau nom du departement
	   */
	  public void setNameDepartment(String paramNameDep) {
		  this.nameDep = paramNameDep;
	  }
	  
	  /**
	   * Ajouter un employé dans la liste
	   * @param EmployeeToAdd objet Employee  ajouté dans la liste
	   */
	  public void addEmployeeToList(Employee paramEmployeeToAdd) {
		  listEmployee.add(paramEmployeeToAdd);
	  }		
	  
	  /**
	   * Supprime un employé de la liste a une position donnée
	   * @param Position (int) La position à laquelle il faut supprimer l'employé
	   */
	  public void delEmployeeFromList(int Position) {
		  listEmployee.remove(Position);
	  }	
	  
	  
	  /**
	   * Retourne la taille de la liste d'Employee
	   * @return (int) - Taille de la liste d'Emloyee
	   */
	  public int getSizeEmployeeList( ) {
		  return listEmployee.size();
	  }
	  
	  /**
	   * Retourne l'employée à la position demandé
	   * @param paramIndex position à laquelle on souhaite obtenir l'employée
	   * @return (Employee) - objet Employee
	   */
	  public Employee getEmployeeAt(int paramIndex) {
		return listEmployee.get(paramIndex);
	  }
	  
	  /**
	   * Retourne la liste des employées 
	   * @return (ArrayList<Employee>) - liste Employee
	   */
	  public ArrayList<Employee> getListEmployee() {
	  	return listEmployee;
	  }
	  
}

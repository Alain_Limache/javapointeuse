package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *  Classe qui stocke 
 *  - Le nom de l'entreprise
 *  - la liste des départements de cette entreprise 
 */

public class Company implements Serializable {
	
	//------ ATTRIBUTS ------//

	private String nameCompany;
	private ArrayList<Department> listDepartment;
	
	
	//------ Constructeurs ------//

	/**
	 *  Contructeur par défaut 
	 * Initialise 
	 * - la liste de département d'une entreprise
	 */
	public Company() 
	{
		listDepartment = new ArrayList<Department>();
	}
	
	/**
	 * Contructeur 
	 * @param CompName le nom de l'entreprise que l'on veut initialiser
	 * crée une entreprise directement avec un nom
	 * crée une liste de département  
	 */
	public Company(String CompName) 
	{
		this.nameCompany = CompName;
		listDepartment = new ArrayList<Department>();
	}
	
	
	//------ METHODES ------//
	
	/**
	* Méthode pour connaître le nom de l'entreprise
	* @return le nom de l'entreprise
	*/
	public String getCompanyName( ) 
	{
		return this.nameCompany;
	}
	
	/**
	 * Ajouter un departement dans la liste
	 * @param paramDepartment objet Department à ajouter dans la liste
	 */
	public void addDepartmentToList(Department paramDepartment)
	{
		listDepartment.add(paramDepartment);
	}
	
	/**
	 * Connaître un département à une position donnée
	 * @param paramIndex la position du département que l'on veut
	 * @return Le departement à la position demandée
	 */
	public Department getDepartment(int paramIndex) {
		return listDepartment.get(paramIndex);
	}
	
	/**
	 * Méthode pour avoir les départements de l'entreprise
	 * @return La liste des departments 
	 */
	public ArrayList<Department> getDepartmentList() {
		return listDepartment;
	}
	
	/**
	 * Méthode pour connaître le nombre de département de l'entreprise
	 * @return size() la taille de la liste des departements
	 */
	public int getSizeDepartmentList() {
		return listDepartment.size();
	}
	
	
}

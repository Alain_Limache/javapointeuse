package Model;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.HashMap;

/**
 *  Classe qui stocke 
 *  - un planning   
 */
public class Planning implements Serializable{
	
	//------ATTRIBUTS------// 
	private HashMap<DAY,HoursOfWork> planning; 
	
	//------CONSTRUCTEURS------//
	
	/**
	* Instancie un nouvel objet Planning 
    */
	public Planning( ) {
		this.planning = new HashMap<DAY,HoursOfWork>();
	}
	
	
	//------METHODES------//
	/**
	 * Si le jour est déjà dans le planning on modifie les horaires 
	 * sinon on ajouter le jour et des horaires 
	 * @param paramDay le jour à instancier
	 * @param paramHours les horaires à instancier
	 */
	public void AddDayOfWork(DAY paramDay, HoursOfWork paramHours) {
		if(!this.planning.containsKey(paramDay)) {
			this.planning.put(paramDay, paramHours);
		}else {
			this.ChangeTimeOfDay(paramDay, paramHours.getBeginTime(),paramHours.getEndingTime());
		}
		
	}
	
	
	/**
	 * Supprimer un jour et ces horaires
	 * @param paramDay le jour à supprimer
	 */
	public void RemoveDayOfWork(DAY paramDay) {
		this.planning.remove(paramDay);
	}
	
	
	/**
	 * Changer l'heure de début du jour
	 * @param paramDay le jour à modifier
	 * @param start l'heure à ajouter
	 */
	public void ChangeStartingTime(DAY paramDay, LocalTime start) {
		this.planning.get(paramDay).setBeginTime(start);
	}
	
	
	/**
	 * Changer l'heure de fin du jour
	 * @param paramDay le jour  modifier
	 * @param end l'heure  ajouter
	 */
	public void ChangeEndingTime(DAY paramDay, LocalTime end) {
		this.planning.get(paramDay).setEndingTime(end);
	}
	
	
	/**
	 * Changer les horaires d'un jour
	 * @param paramDay le jour à modifier
	 * @param start l'heure à ajouter
	 * @param end l'heure à ajouter
	 */
	public void ChangeTimeOfDay(DAY paramDay, LocalTime start, LocalTime end) {
		this.ChangeStartingTime(paramDay,start);
		this.ChangeEndingTime(paramDay,end);
	}
	
	
	/**
	 * Instancie un planning par défaut
	 */
	public void PlanningByDefault() {
		LocalTime start = LocalTime.of(9,0);
		LocalTime ending = LocalTime.of(17,0);
		HoursOfWork hours = new HoursOfWork(start,ending);
		this.AddDayOfWork(DAY.MONDAY, hours);
		this.AddDayOfWork(DAY.TUESDAY, hours);
		this.AddDayOfWork(DAY.WEDNESDAY, hours);
		this.AddDayOfWork(DAY.THURSDAY, hours);
		this.AddDayOfWork(DAY.FRIDAY, hours);
	}
	
}

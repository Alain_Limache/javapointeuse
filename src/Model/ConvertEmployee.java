package Model;

import java.io.Serializable;

/**
*  Classe qui stocke 
*  - le nom
*  - le prenom 
*  - l'id d'un employé à envoyer à la pointeuse
*/
public class ConvertEmployee implements Serializable{

	 //------ ATTRIBUTS ------//
	private static final long serialVersionUID = 1L;
	private int userID;
	private String lastName;
	private String firstName;
	
	//------ CONSTRUCTEURS ------//
	public ConvertEmployee(Employee paramEmployee) {
		setFirstName(paramEmployee.getFirstName());
		setLastName(paramEmployee.getName());
		setUserID(paramEmployee.getUserID());
	}
	
	public ConvertEmployee(int paramID, String paramLastName, String paramFirstName) {
		setFirstName(paramFirstName);
		setLastName(paramLastName);
		setUserID(paramID);
	}

	 //------ METHODES ------//
	
	/**
	* Avoir l'id de l'employée
	* @return (int) - L'id de l'employée
	*/
	public int getUserID() {
		return userID;
	}

	/**
	* Modifier l'id de l'employée
	*/
	public void setUserID(int userID) {
		this.userID = userID;
	}

	
	/**
	* Avoir le nom de l'employée
	* @return (String) - Nom de l'employée
	*/
	public String getLastName() {
		return lastName;
	}

	/**
	* Modifier le nom de l'employée
	*/
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	* Avoir le prénom de l'employée
	* @return (String) - Prénom de l'employée
	*/
	public String getFirstName() {
		return firstName;
	}

	/**
	* Modifier le prénom de l'employée
	*/
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	* Afficher le nom et prénom de l'employée
	* @return (String) - le nom et prénom de l'employée
	*/
	public String toString() {
		return this.getFirstName() + " " + this.getLastName() + " " + this.getUserID();
	}
	
}
